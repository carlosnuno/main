# description	    : Music playback daemon
# homepage	    : https://www.musicpd.org/
# depends	    : boost fmt pcre2 meson ninja alsa-lib libsndfile ffmpeg
# optional	    : audiofile avahi dbus faad2 flac fluidsynth icu jack lame libcdio-paranoia
# optional	    : libid3tag libisofs libmad libmpcdec libsamplerate mpg123
# optional	    : pipewire pulseaudio python3-sphinx samba twolame yajl
# optional	    : libsoxr wavpack

name=mpd
version=0.23.15
release=3
source="https://www.musicpd.org/download/$name/0.23/$name-$version.tar.xz
	fmt.patch
	rc.mpd"

build() {

    patch -Np1 -d $name-$version -i $SRC/fmt.patch
  venom-meson $name-$version build  \
    --auto-features disabled \
    -D curl=enabled \
    -D bzip2=enabled \
    -D iconv=enabled \
    -D ipv6=enabled \
    -D pcre=enabled \
    -D sqlite=enabled \
    -D alsa=enabled \
    -D sndfile=enabled \
    -D audiofile=$(scratch isinstalled audiofile && echo enabled || echo disabled) \
    -D dbus=$(scratch isinstalled dbus && echo enabled || echo disabled) \
    -D faad=$(scratch isinstalled faad2 && echo enabled || echo disabled) \
    -D flac=$(scratch isinstalled flac && echo enabled || echo disabled) \
    -D fluidsynth=$(scratch isinstalled fluidsynth && echo enabled || echo disabled) \
    -D icu=$(scratch isinstalled icu && echo enabled || echo disabled) \
    -D jack=$(scratch isinstalled jack && echo enabled || echo disabled) \
    -D lame=$(scratch isinstalled lame && echo enabled || echo disabled) \
    -D cdio_paranoia=$(scratch isinstalled libcdio-paranoia  && echo enabled || echo disabled) \
    -D id3tag=$(scratch isinstalled libid3tag && echo enabled || echo disabled) \
    -D iso9660=$(scratch isinstalled libisofs && echo enabled || echo disabled) \
    -D mad=$(scratch isinstalled libmad && echo enabled || echo disabled) \
    -D libsamplerate=$(scratch isinstalled libsamplerate && echo enabled || echo disabled) \
    -D soxr=$(scratch isinstalled libsoxr && echo enabled || echo disabled) \
    -D mpcdec=$(scratch isinstalled libmpcdec && echo enabled || echo disabled) \
    -D mpg123=$(scratch isinstalled mpg123 && echo enabled || echo disabled) \
    -D pulse=$(scratch isinstalled pulseaudio && echo enabled || echo disabled) \
    -D pipewire=$(scratch isinstalled pipewire && echo enabled || echo disabled) \
    -D smbclient=$(scratch isinstalled samba && echo enabled || echo disabled) \
    -D twolame=$(scratch isinstalled twolame && echo enabled || echo disabled) \
    -D wavpack=$(scratch isinstalled wavpack && echo enabled || echo disabled) \
    -D yajl=$(scratch isinstalled yajl && echo enabled || echo disabled) \
    -D html_manual=false
  meson compile -C build 
  DESTDIR=$PKG meson install --no-rebuild -C build

  install -Dm 0644 $SRC/$name-$version/doc/mpdconf.example $PKG/etc/mpd/mpd.conf
  install -D $SRC/rc.mpd $PKG/etc/rc.d/mpd

  rm -r $PKG/usr/share/doc
}

