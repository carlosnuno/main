#!/bin/sh

kernver=$(find /lib/modules -maxdepth 1 -type d -iname '*-linux' -printf "%f\n")

if [ $(command -v mkinitramfs) ]; then
	echo "mkinitramfs: generating initramfs for kernel $kernver..."
	mkinitramfs -q -k $kernver -o /boot/initrd-linux.img
fi

# removing other venom's kernel
for i in /lib/modules/*; do
	[ -d $i ] || continue
	case ${i##*/} in
		$kernver) continue;;
		*-linux|*-Venom)
			[ -d $i/build/include ] && continue
			echo "post-install: removing kernel ${i##*/}"
			rm -fr $i;;
	esac
done

rm -f /boot/config-venom /boot/vmlinuz-venom /boot/initrd-venom.img

# load kernel modules
depmod $kernver

# run all dkms scripts
if [ $(command -v dkms) ]; then
	for i in /var/lib/dkms/buildmodules-*.sh; do
	    [ -f $i ] && sh $i
	done
fi

# reconfigure grub
if [ $(command -v grub-mkconfig) ]; then
	echo "grub-mkconfig: reconfigure for kernel $kernver..."
	grub-mkconfig -o /boot/grub/grub.cfg
fi

