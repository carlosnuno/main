# description	: FFmpeg is a solution to record, convert and stream audio and video
# homepage	: https://ffmpeg.org/
# depends	: libass fdk-aac freetype2 lame gnutls libtheora libvorbis libvpx opus x264 x265 alsa-lib sdl2 yasm aom dav1d

name=ffmpeg6
_name=ffmpeg
version=6.1.1
release=5
source="https://ffmpeg.org/releases/$_name-$version.tar.xz
	ffmpeg-${version}-chromium_method-1.patch
	vulkan-fixes-1.patch"

build() {
	cd $_name-$version

	export CFLAGS="$CFLAGS -Wno-incompatible-pointer-types"

	_opt=""

	addopt() {
		if [ "$_opt" = "" ]; then
			scratch isinstalled $1 && _opt="--enable-$2" || _opt="--disable-$2"
		elif [ ! "$_opt" = "" ]; then
			scratch isinstalled $1 && _opt="$_opt --enable-$2" || _opt="$_opt --disable-$2"
		fi
	}

	## $1 is package name
	## $2 is feature
	addopt libvdpau vdpau
	addopt libva vaapi
	addopt nvidia cuda
	addopt nvidia cuvid 
	addopt nv-codec-headers nvenc
	addopt amf-headers amf
	addopt svt-av1 libsvtav1
	addopt vulkan-icd-loader vulkan
	
	# vulkan patch
	patch -Np1 -i $SRC/vulkan-fixes-1.patch

	# Adds an API necessary for some packages to build
	patch -Np1 -i $SRC/ffmpeg-${version}-chromium_method-1.patch

	# adds the ALSA library to the Flite LDFLAGS variable and
	# enables the discovery of Flite
	sed -i 's/-lflite"/-lflite -lasound"/' configure

	./configure --prefix=/usr        \
		    --incdir=/usr/include/$name \
		    --libdir=/usr/lib/$name \
	            --enable-gpl         \
		    --enable-gnutls      \
	            --enable-version3    \
	            --enable-nonfree     \
	            --disable-static     \
		    --enable-lto         \
	            --enable-shared      \
	            --disable-debug      \
		    --disable-libcelt    \
		    --disable-optimizations \
		    --disable-stripping  \
		    --disable-doc  \
		    --enable-swresample  \
		    --enable-avfilter    \
		    --enable-pic         \
		    --enable-postproc    \
		    --enable-pthreads    \
	            --enable-libass      \
	            --enable-libfdk-aac  \
	            --enable-libfreetype \
	            --enable-libmp3lame  \
	            --enable-libopus     \
	            --enable-libtheora   \
	            --enable-libvorbis   \
	            --enable-libvpx      \
	            --enable-libx264     \
	            --enable-libx265	 \
	            --enable-libaom      \
		    --enable-libdav1d    \
		    $_opt                
	make
	gcc tools/qt-faststart.c -o tools/qt-faststart
	make DESTDIR=$PKG install

	# Move libs to /usr/lib, except the .so symlinks
	for f in usr/lib/${name}/*; do
		if [[ $f == *.so ]]; then
		      ln -srf -- usr/lib/"$(readlink "$f")" "$f"
		elif [[ ! -d $f ]]; then
		mv "$f" usr/lib
		fi
	done

	install -m755 tools/qt-faststart $PKG/usr/bin

	install -d $PKG/etc/ld.so.conf.d
	echo "/usr/lib/${name}" > $PKG/etc/ld.so.conf.d/${name}.conf

	rm -rf $PKG/usr/bin $PKG/usr/share
}
