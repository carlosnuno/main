# description	: Open-source OpenGL drivers
# homepage	: https://www.mesa3d.org/
# depends	: llvm python3-mako elfutils bison flex libclc libxext libxdamage python3-ply python3-yaml
# depends	: libxshmfence libxxf86vm libxrandr libdrm libglvnd glslang rust cbindgen rust-bindgen
# optional	: libva libvdpau wayland-protocols

SYN_PV=2.0.68
PROC_MACRO2_PV=1.0.86
QUOTE_PV=1.0.33
UNICODE_IDENT_PV=1.0.12
PASTE_PV=1.0.14

name=mesa
version=24.2.2
release=1
source="https://mesa.freedesktop.org/archive/$name-$version.tar.xz
	syn-${SYN_PV}.tar.gz::https://github.com/dtolnay/syn/archive/refs/tags/${SYN_PV}.tar.gz
	proc-macro2-${PROC_MACRO2_PV}.tar.gz::https://github.com/dtolnay/proc-macro2/archive/refs/tags/${PROC_MACRO2_PV}.tar.gz
	quote-${QUOTE_PV}.tar.gz::https://github.com/dtolnay/quote/archive/refs/tags/${QUOTE_PV}.tar.gz
	unicode-ident-${UNICODE_IDENT_PV}.tar.gz::https://github.com/dtolnay/unicode-ident/archive/refs/tags/${UNICODE_IDENT_PV}.tar.gz
	paste-${PASTE_PV}.tar.gz::https://github.com/dtolnay/paste/archive/refs/tags/${PASTE_PV}.tar.gz
	rust-bindgen-0.70.patch"

build() {
	cd $name-$version

	patch -Np1 -i $SRC/rust-bindgen-0.70.patch

	for subpkg in proc-macro2-${PROC_MACRO2_PV} syn-${SYN_PV} quote-${QUOTE_PV} unicode-ident-${UNICODE_IDENT_PV} paste-${PASTE_PV}; do
		#copy subprojects folder
		cp -r ../"${subpkg}" subprojects || die
		# copy meson.build
		cp subprojects/packagefiles/"${subpkg%-*}"/meson.build subprojects/"${subpkg}" || die
		# ovewrite subpkg version when needed
		sed -i -e "s/directory = \S\+/directory = ${subpkg}/" subprojects/"${subpkg%-*}".wrap || die
	done

	scratch isinstalled vulkan-icd-loader && OPT_MESA_GALLIUM='zink,'
	scratch isinstalled libvdpau && OPT_MESA_VDPAU='-Dgallium-vdpau=enabled' || OPT_MESA_VDPAU='-Dgallium-vdpau=disabled'
	scratch isinstalled libva && OPT_MESA_VAAPI='-Dgallium-va=enabled' || OPT_MESA_VAAPI='-Dgallium-va=disabled'
	scratch isinstalled wayland-protocols && OPT_MESA_PLATFORMS='wayland,x11' || OPT_MESA_PLATFORMS='x11'

	venom-meson build \
		-Db_lto=false \
		-Ddri3=enabled \
		-Degl=enabled \
		-Dllvm=enabled \
		-Dshared-llvm=enabled \
		-Dgbm=enabled \
		-Dgles1=disabled \
		-Dgles2=enabled \
		-Dglx=dri \
		-Dosmesa=true \
		-Dgallium-xa=enabled \
		-Dgallium-drivers=${OPT_MESA_GALLIUM}crocus,iris,nouveau,r300,r600,radeonsi,svga,swrast,virgl,i915 \
		-Dplatforms=${OPT_MESA_PLATFORMS} \
		-Dintel-clc=enabled \
		-Dintel-rt=enabled \
		-Dgallium-opencl=icd \
		-Dgallium-rusticl=true \
		-Dshared-glapi=enabled \
		-Dvulkan-drivers=amd,intel,intel_hasvk,nouveau \
		-Dvulkan-layers=device-select,intel-nullhw,overlay \
		-Dvideo-codecs=vc1dec,h264dec,h264enc,h265dec,h265enc \
		-Dglvnd=enabled	$OPT_MESA_VDPAU $OPT_MESA_VAAPI
	meson compile -C build
	DESTDIR=$PKG meson install --no-rebuild -C build

	# indirect rendering symlink
	ln -s libGLX_mesa.so.0 $PKG/usr/lib/libGLX_indirect.so.0
}

