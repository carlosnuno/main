#!/bin/sh

kernver=$(find /lib/modules -maxdepth 1 -type d -iname '*-linux-lts' -printf "%f\n")

if [ $(command -v mkinitramfs) ]; then
	echo "mkinitramfs: generating initramfs for kernel $kernver..."
	mkinitramfs -q -k $kernver -o /boot/initrd-linux-lts.img
fi

# removing other venom's kernel
for i in /lib/modules/*; do
	[ -d $i ] || continue
	case ${i##*/} in
		$kernver) continue;;
		*-linux-lts|*-Venom-LTS)
			[ -d $i/build/include ] && continue
			echo "post-install: removing kernel ${i##*/}"
			rm -fr $i;;
	esac
done

rm -f /boot/config-venom-lts /boot/vmlinuz-venom-lts /boot/initrd-venom-lts.img

# load kernel modules
depmod $kernver

# run all dkms scripts
if [ $(command -v dkms) ]; then
	for i in /var/lib/dkms/buildmodules-*.sh; do
	    [ -f $i ] && sh $i
	done
fi

# reconfigure grub
if [ $(command -v grub-mkconfig) ]; then
	echo "grub-mkconfig: reconfigure for kernel $kernver..."
	grub-mkconfig -o /boot/grub/grub.cfg
fi

