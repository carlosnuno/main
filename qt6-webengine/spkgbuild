# description	: Provides support for web applications using the Chromium browser project
# homepage	: https://www.qt.io/
# depends	: ffmpeg icu libevent libva libvpx libwebp minizip nodejs nss pciutils python3-html5lib python3-six 
# depends	: qt6-tools qt6-webchannel pcre2 libxcomposite libxcursor libxkbfile brotli
# optional	: dav1d fdk-aac gnutls graphite2 keyutils krb5 lame lcms2 libvdpau numactl pipewire x264 x265 pipewire

name=qt6-webengine
version=6.7.2
release=2
source="$name-$version.tar.xz::https://download.qt.io/official_releases/qt/${version%.*}/$version/submodules/qtwebengine-everywhere-src-$version.tar.xz
	qtwebengine-6.7.0-ninja1.12.patch
	qtwebengine-6.7.2-ffmpeg7.patch"

build() {
	cd qtwebengine-everywhere-src-$version

	export PYTHON=/usr/bin/python3 

	# fix build with ninja 1.12
	patch -Np1 -i $SRC/qtwebengine-6.7.0-ninja1.12.patch

	# fix build with ffmpeg 7
	patch -Np1 -d src/3rdparty/chromium < $SRC/qtwebengine-6.7.2-ffmpeg7.patch

	# fix a build of ffmpeg does not use OpenH264
	sed -e '189 s/=/& false/' \
	    -e '190 d'            \
	    -i.orig src/3rdparty/chromium/third_party/webrtc/webrtc.gni

	cmake -B build -G Ninja \
		-D QT_USE_CCACHE=$(scratch isinstalled ccache && echo ON || echo OFF) \
		-DCMAKE_MESSAGE_LOG_LEVEL=STATUS \
		-DCMAKE_TOOLCHAIN_FILE=/usr/lib/cmake/Qt6/qt.toolchain.cmake \
		-DQT_FEATURE_webengine_system_ffmpeg=ON \
		-DQT_FEATURE_webengine_system_icu=ON \
		-DQT_FEATURE_webengine_system_libevent=ON \
		-DQT_FEATURE_webengine_proprietary_codecs=ON \
		-D QT_FEATURE_webengine_ozone_x11=ON \
		-D QT_FEATURE_webengine_system_re2=OFF \
		-DQT_FEATURE_webengine_kerberos=$(scratch isinstalled krb5 && echo ON || echo OFF) \
		-DQT_FEATURE_webengine_webrtc_pipewire=$(scratch isinstalled pipewire && echo ON || echo OFF)
	cmake --build build
	DESTDIR=$PKG cmake --install build
}
