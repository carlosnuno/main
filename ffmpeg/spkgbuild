# description	: FFmpeg is a solution to record, convert and stream audio and video
# homepage	: https://ffmpeg.org/
# depends	: libass fdk-aac freetype2 lame gnutls libtheora libvorbis libvpx opus x264 x265 
# depends	: alsa-lib sdl2 yasm aom dav1d

name=ffmpeg
version=7.0.2
release=3
source="https://ffmpeg.org/releases/$name-$version.tar.xz
	ffmpeg-x264-10bit.sh
	add-av_stream_get_first_dts-for-chromium.patch
	vaapi-decode-send-multiple-slice.patch
	vaapi-av1-avoid-slice-buffer-multiple.patch"

build() {
	cd $name-$version

	export CFLAGS="$CFLAGS -Wno-incompatible-pointer-types" 

	# https://git.ffmpeg.org/gitweb/ffmpeg.git/patch/fe9d889dcd79ea18d4dfaa39df4ddbd4c8c3b15c
	patch -Np1 -i $SRC/vaapi-decode-send-multiple-slice.patch
	
	# https://git.ffmpeg.org/gitweb/ffmpeg.git/patch/d2d911eb9a2fc6eb8d86b3ae025a56c1a2692fba
	patch -Np1 -i $SRC/vaapi-av1-avoid-slice-buffer-multiple.patch

	_opt=""

	addopt() {
		if [ "$_opt" = "" ]; then
			scratch isinstalled $1 && _opt="--enable-$2" || _opt="--disable-$2"
		elif [ ! "$_opt" = "" ]; then
			scratch isinstalled $1 && _opt="$_opt --enable-$2" || _opt="$_opt --disable-$2"
		fi
	}

	## $1 is package name
	## $2 is feature
	addopt libvdpau vdpau
	addopt libva vaapi
	addopt nvidia cuda
	addopt nvidia cuvid 
	addopt nv-codec-headers nvenc
	addopt amf-headers amf
	addopt svt-av1 libsvtav1
	addopt vulkan-icd-loader vulkan

	# Adds an API necessary for some packages to build
	patch -Np1 -i $SRC/add-av_stream_get_first_dts-for-chromium.patch

	# adds the ALSA library to the Flite LDFLAGS variable and
	# enables the discovery of Flite
	sed -i 's/-lflite"/-lflite -lasound"/' configure

	./configure --prefix=/usr        \
	            --enable-gpl         \
		    --enable-gnutls      \
	            --enable-version3    \
	            --enable-nonfree     \
	            --disable-static     \
		    --enable-lto         \
	            --enable-shared      \
	            --disable-debug      \
		    --disable-libcelt    \
		    --disable-optimizations \
		    --disable-stripping  \
		    --disable-doc  \
		    --enable-swresample  \
		    --enable-avfilter    \
		    --enable-pic         \
		    --enable-postproc    \
		    --enable-pthreads    \
	            --enable-libass      \
	            --enable-libfdk-aac  \
	            --enable-libfreetype \
	            --enable-libmp3lame  \
	            --enable-libopus     \
	            --enable-libtheora   \
	            --enable-libvorbis   \
	            --enable-libvpx      \
	            --enable-libx264     \
	            --enable-libx265	 \
	            --enable-libaom      \
		    --enable-libdav1d    \
		    $_opt                
	make
	gcc tools/qt-faststart.c -o tools/qt-faststart
	make DESTDIR=$PKG install

	install -m755 tools/qt-faststart $PKG/usr/bin

	[ -e '/usr/lib/pkgconfig/x264.pc' ] && \
		install -m 0755 -D $SRC/ffmpeg-x264-10bit.sh \
			$PKG/usr/bin/ffmpeg-x264-10bit

	rm -r $PKG/usr/share/ffmpeg/examples
}
